use proc_macro::TokenTree::Group;
use proc_macro::TokenTree::Punct;
// use proc_macro::Group;
use proc_macro::TokenStream;
use proc_macro::TokenTree::Ident;

#[proc_macro_derive(read_search)]
pub fn read_search(item: TokenStream) -> TokenStream {
    // println!("{:?}", item);
    let mut result = String::new();
    result.push_str("impl ");
    let mut iter_stream = item.into_iter();

    let mut close_state_machine = false;
    while !close_state_machine {
        match iter_stream.next().expect("Failed to get first element") {
            Ident(x) if x.to_string() == "pub" => println!("Ignoring pub modifier"),
            Ident(x) if x.to_string() == "struct" => {
                result.push_str(&iter_stream.next().unwrap().to_string());
                result.push_str(" {\n");

                if let Group(x) = &iter_stream.next().unwrap() {
                    // println!("{:?}", x);
                    // println!("{:?}", parse_group(x.stream()));
                    result.push_str(&parse_group(x.stream()));
                }
                result.push_str("}\n");
                close_state_machine = true;
            }
            _ => panic!("first element is not struct..."),
        }
    }

    println!("{}", &result);
    result.parse().unwrap()
    // "pub fn read_search() -> u32 {42}".parse().unwrap()
}

fn parse_group(stream: TokenStream) -> String {
    let mut result = String::new();
    let mut key_term = Vec::<(String, String)>::new();

    let mut stream = stream.into_iter();
    while let Some(ident) = stream.next() {
        // println!(ident);
        let identifier = ident.to_string();
        expect_punctuation(&mut stream, ':').expect("incorrect separator");
        if let Some(r#type) = stream.next() {
            let r#type = r#type.to_string();
            expect_punctuation(&mut stream, ',').unwrap_or(());
            key_term.push((identifier, r#type));
        } else {
            panic!("aaa");
        }
    }

    // for kt in key_term {
    //     println!("Ident: {}, Type: {}", kt.0, kt.1);
    // }

    result.push_str("pub fn gen_parse(value: Value) -> Self {\n");
    result.push_str("if let Value::Struct(value) = value {\n");
    result.push_str("if let (\n");
    for kt in &key_term {
        result.push_str(&format!("{}({})\n,", get_odoo_type(&kt.1), kt.0))
    }
    result.push_str(") = (");
    for kt in &key_term {
        result.push_str(&format!(
            "value.get(\"{}\").expect(\"Failed to find {} in response\"),\n",
            kt.0, kt.0
        ));
    }
    result.push_str(") {\n");
    result.push_str("Self {\n");
    for kt in &key_term {
        result.push_str(&format!(
            "{}: {},\n",
            kt.0,
            get_deref_for_type(&kt.1, &kt.0)
        ));
    }
    result.push_str("}} else { panic! (\"meh\");}} else { panic!(\"Aaa\");}}");
    // result.push_str("}\n");
    result
}

fn get_deref_for_type(rust_type: &str, ident: &str) -> String {
    match rust_type {
        "bool" | "f64" | "i32" => format!("*{}", ident),
        "String" => format!("{}.clone()", ident),
        x => panic!("Unknown odoo type to find deref from {}", x),
    }
}

fn get_odoo_type(rust_type: &str) -> &'static str {
    match rust_type {
        "bool" => "Value::Bool",
        "f64" => "Value::Double",
        "i32" => "Value::Int",
        "String" => "Value::String",
        x => panic!("Unknown odoo type to convert from {}", x),
    }
}

fn expect_punctuation(stream: &mut proc_macro::token_stream::IntoIter, ch: char) -> Result<(), ()> {
    if let Some(Punct(pct)) = stream.next() {
        if pct.as_char() == ch {
            Ok(())
        } else {
            Err(())
        }
    } else {
        Err(())
    }
}
// #[cfg(test)]
// mod tests {
//     #[derive(AnswerFn)]
//     struct Dummy {
//         id: i32,
//         name: String,
//         age: f64
//     }
//     #[test]
//     fn it_works() {
//         assert_eq!(Dummy::read_search(), 42)
//     }
// }
